package com.chilima.patientrecord;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiService {
    @GET("index")
    Call <List<Patient>> getPatients();
}
