package com.chilima.patientrecord;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PatientActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ApiService api = RetrofitClient.getApiService();

        Call<List<Patient>> call = api.getPatients();

        call.enqueue(new Callback<List<Patient>>() {
            @Override
            public void onResponse(Call<List<Patient>> call, Response<List<Patient>> response) {

                if (response.isSuccessful()) {

                    List<Patient> patients = response.body();

                    Log.i("EK", patients.get(0).address.toString());
                    showListOfPatients(patients);
                } else {
                    Log.i("EK", "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
                }
            }
            @Override
            public void onFailure(Call<List<Patient>> call, Throwable t) {

                t.printStackTrace();
// Handle the network or request failure
            }
        });

    }

    private void showListOfPatients(List<Patient> patientList) {
        for(Patient p : patientList){
            Log.i("EK", "==================================");
            Log.i("EK", p.name);
            Log.i("EK", p.age+"");
            Log.i("EK", p.address);
            Log.i("EK", "==================================");

        }
    }
}